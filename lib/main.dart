import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:submager/screens/style.dart';
import 'package:submager/widgets/screens/cycle_paymnet_screen/cycle_payment_screen.dart';
import 'package:submager/widgets/screens/home_screen/home_screen.dart';
import 'package:submager/widgets/screens/subscriptions_screen/subscriptions_screen.dart';

void main() {
  // WidgetsFlutterBinding.ensureInitialized();
  // // Firebase.initializeApp().then((value) {
    runApp(MyApp());
  // });
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          // primarySwatch: MaterialColor( ),
          primaryColor: Colors.white,
          backgroundColor: Colors.white30,
          textTheme: TextTheme(
            headline1: Style.submagerTextStyle(),
            headline2: Style.submagerTextStyle(),
            headline3: Style.submagerTextStyle(),
            headline4: Style.submagerTextStyle(),
            headline5: Style.submagerTextStyle(bold: true),
            headline6: Style.submagerTextStyle(),
          )),
      routes: {
        "/": (BuildContext context) => HomeScreen(),
        "cycle-payments-screen": (BuildContext context) => CyclePaymentScreen(),
        "subscritptions-screen": (BuildContext context) =>
            SubscriptionsScreen(),
      },
    );
  }
}
