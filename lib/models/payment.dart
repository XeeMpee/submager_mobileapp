import 'package:cloud_firestore/cloud_firestore.dart';

class Payment {
  Payment({this.reference});
  String? invoiceUrl;
  String? owner;
  String? payTo;
  String? price;
  String? sourceId;
  String? status;
  String? type;

  DocumentReference? reference;

  static Payment fromJson(dynamic data) {
    return Payment()
      ..invoiceUrl = data["invoiceUrl"]
      ..owner = data["owner"]
      ..payTo = data["payTo"]
      ..price = data["price"]
      ..sourceId = data["sourceId"]
      ..status = data["status"]
      ..type = data["type"];
  }

  Map<String, dynamic> toJson() {
    return {
      "invoiceUrl": invoiceUrl,
      "owner": owner,
      "payTo": payTo,
      "price": price,
      "sourceId": sourceId,
      "status": status,
      "type": type,
    };
  }
}
