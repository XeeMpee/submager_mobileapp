import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:submager/models/payment.dart';

class PaymentsRepository {
  // 1
  final CollectionReference collection = FirebaseFirestore.instance.collection('payments');
  // 2
  Stream<QuerySnapshot> getStream() {
    return collection.snapshots();
  }
  // 3
  Future<DocumentReference> addPet(Payment payment) {
    return collection.add(payment.toJson());
  }
  // 4
  updatePet(Payment payment) async {
    // await collection.document(payment.reference!.documentID).updateData(payment.toJson());
  }
}