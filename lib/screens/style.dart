import 'package:flutter/rendering.dart';

class Style {
  static TextStyle submagerTextStyle({bool bold = false}) {
    return TextStyle(
        fontFamily: "PkoFont",
        fontWeight: bold == true ? FontWeight.bold : FontWeight.normal);
  }
}
