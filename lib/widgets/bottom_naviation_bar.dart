import 'package:flutter/material.dart';

class SubmagerBottomNavigationBar extends StatefulWidget {
  const SubmagerBottomNavigationBar({Key? key, required this.onChoose }) : super(key: key);
  final void Function(int index) onChoose;

  @override
  _SubmagerBottomNavigationBarState createState() =>
      _SubmagerBottomNavigationBarState();
}

class _SubmagerBottomNavigationBarState
    extends State<SubmagerBottomNavigationBar> {
  
  int _selectedIndex = 1;
  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      // fixedColor: Colors.black,
      unselectedItemColor: Colors.black,
      selectedItemColor: Colors.blue[900],
      selectedLabelStyle:
          TextStyle(fontFamily: "PkoFont", color: Colors.blue[900]),
      unselectedLabelStyle:
          TextStyle(fontFamily: "PkoFont", color: Colors.black),
      showUnselectedLabels: true,
      currentIndex: _selectedIndex,
      items: [
        BottomNavigationBarItem(
          icon: Icon(Icons.home_outlined),
          label: "Start",
        ),
        BottomNavigationBarItem(
            icon: Icon(Icons.compare_arrows_rounded), label: "Płatnosci"),
        BottomNavigationBarItem(
            icon: Icon(Icons.account_balance_wallet_outlined),
            label: "Produkty"),
        BottomNavigationBarItem(
            icon: Icon(Icons.badge_outlined), label: "Oferta"),
        BottomNavigationBarItem(icon: Icon(Icons.more_horiz), label: "Wiecej"),
      ],
      onTap: _onItemTapped,
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      widget.onChoose(index);
    });
  }
}
