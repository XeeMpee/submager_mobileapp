
import 'package:flutter/material.dart';

class ChooseButtonWidget extends StatefulWidget {
  const ChooseButtonWidget({Key? key, required this.text, required this.onTap})
      : super(key: key);

  final String text;
  final Function onTap;

  @override
  _ChooseButtonWidgetState createState() => _ChooseButtonWidgetState();
}

class _ChooseButtonWidgetState extends State<ChooseButtonWidget> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => widget.onTap(),
      child: Container(
        margin: EdgeInsets.all(10),
        child: Container(
          height: 80,
          width: 70,
          child: Column(
            children: [
              CircleAvatar(
                foregroundColor: Colors.blue[200],
              ),
              Text(
                widget.text,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: "PKOFont",
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
