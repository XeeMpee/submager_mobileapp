import 'package:flutter/widgets.dart';

class LineConstantGrid extends StatelessWidget {
  const LineConstantGrid(
      {Key? key, required this.lineLimit, required this.children})
      : super(key: key);
  final int lineLimit;
  final List<Widget> children;

  @override
  Widget build(
    BuildContext context,
  ) {
    List<Row> lines = [];
    int rowsAmount = (children.length / this.lineLimit).floor();
    if (children.length % this.lineLimit != 0) {
      rowsAmount += 1;
    }
    for (int i = 0; i < rowsAmount; i++) {
      lines.add(Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: children.sublist(i * lineLimit, i*lineLimit + lineLimit < children.length ? i+lineLimit : children.length)));
    }
    return Container(
      alignment: Alignment.center,
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: lines),
    );
  }
}
