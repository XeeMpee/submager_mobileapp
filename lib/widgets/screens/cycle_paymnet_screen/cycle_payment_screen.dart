import 'package:flutter/material.dart';
import 'package:submager/widgets/bottom_naviation_bar.dart';
import 'package:submager/widgets/screens/home_screen/subscreens/payment_subscreen.dart';
import 'package:submager/widgets/screens/home_screen/subscreens/start_subscreen.dart';

class CyclePaymentScreen extends StatefulWidget {
  CyclePaymentScreen({
    Key? key,
  }) : super(key: key);

  @override
  _CyclePaymentScreenState createState() => _CyclePaymentScreenState();
}

class _CyclePaymentScreenState extends State<CyclePaymentScreen> {
  List<Widget> subscreens = [
    StartSubscreen(),
    PaymentSubscreen(),
    StartSubscreen(),
    StartSubscreen(),
    StartSubscreen(),
  ];
  int currentSubscreenIndex = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "Płatności cykliczne",
          style: Theme.of(context).textTheme.headline6,
        ),
      ),
      bottomNavigationBar: SubmagerBottomNavigationBar(
        onChoose: (int index) {
          setState(() {
            currentSubscreenIndex = index;
          });
        },
      ),
      body: Container(
       
      ),
    );
  }
}
