import 'package:flutter/material.dart';
import 'package:submager/widgets/bottom_naviation_bar.dart';
import 'package:submager/widgets/screens/home_screen/subscreens/payment_subscreen.dart';
import 'package:submager/widgets/screens/home_screen/subscreens/start_subscreen.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({
    Key? key,
  }) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<Widget> subscreens = [
    StartSubscreen(),
    PaymentSubscreen(),
    StartSubscreen(),
    StartSubscreen(),
    StartSubscreen(),
  ];
  int currentSubscreenIndex = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "Start",
          style: Theme.of(context).textTheme.headline6,
        ),
      ),
      bottomNavigationBar: SubmagerBottomNavigationBar(
        onChoose: (int index) {
          setState(() {
            currentSubscreenIndex = index;
          });
        },
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: subscreens[currentSubscreenIndex],
      ),
    );
  }
}
