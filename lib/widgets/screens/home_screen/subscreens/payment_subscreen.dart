import 'package:flutter/material.dart';
import 'package:submager/widgets/choose_button_widget.dart';
import 'package:submager/widgets/line_constant_grid.dart';

class PaymentSubscreen extends StatefulWidget {
  PaymentSubscreen({
    Key? key,
  }) : super(key: key);

  @override
  _PaymentSubscreenState createState() => _PaymentSubscreenState();
}

class _PaymentSubscreenState extends State<PaymentSubscreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        children: <Widget>[
          _buildTitleWdget(context, text: "Przelewy"),
          __buildTransfersGrid(context),
          _buildTitleWdget(context, text: "Płatności mobilne"),
          __builMobilePaymentsGrid(context),
          _buildTitleWdget(context, text: "Inne operacje"),
          __builOtherOperationsPaymentsGrid(context),
        ],
      ),
    );
  }

  Widget _buildTitleWdget(BuildContext context, {required String text}) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(10),
      color: Colors.black12,
      child: Text(
        text,
        style: Theme.of(context).textTheme.headline6,
      ),
    );
  }

  Widget _buildGridWidget(BuildContext context,
      {required double height, required List<Widget> children}) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: height,
      padding: EdgeInsets.all(20),
      child: GridView(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 4, crossAxisSpacing: 10, mainAxisSpacing: 10),
          children: children),
    );
  }

  Widget __buildTransfersGrid(BuildContext context) {
    return LineConstantGrid(
        lineLimit: 4,
        children: [
          "Przelew\n na konto",
          "Przelew\n własny",
          "Przelew\n na telefon",
          "Skanuj i zapłać",
          "Zlecenia stałe",
          "Seria przelewów",
        ]
            .map((String e) => ChooseButtonWidget(text: e, onTap: () {}))
            .toList());
  }

  Widget __builMobilePaymentsGrid(BuildContext context) {
    List<Widget> list = [
      "Przelew\n na konto",
      "Przelew\n własny",
      "Przelew\n na telefon",
      "Skanuj i zapłać",
      "Zlecenia stałe",
    ].map((String e) => ChooseButtonWidget(text: e, onTap: () {})).toList();
    list.add(ChooseButtonWidget(
        text: "Płatności cykliczne",
        onTap: () {
          Navigator.of(context).pushNamed('cycle-payments-screen');
        }));
    list.add(ChooseButtonWidget(
        text: "Subskrypcje",
        onTap: () {
          Navigator.of(context).pushNamed('subscritptions-screen');
        }));
    return LineConstantGrid(lineLimit: 4, children: list);
  }

  Widget __builOtherOperationsPaymentsGrid(BuildContext context) {
    return LineConstantGrid(
        lineLimit: 4,
        children: [
          "Prośba o ",
          "Skanuj kod QR",
          "Polecenia",
          "Doładowania",
        ]
            .map((String e) => ChooseButtonWidget(text: e, onTap: () {}))
            .toList());
  }
}
