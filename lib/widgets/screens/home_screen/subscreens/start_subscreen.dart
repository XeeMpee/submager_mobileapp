import 'package:flutter/material.dart';

class StartSubscreen extends StatefulWidget {
  StartSubscreen({
    Key? key,
  }) : super(key: key);

  @override
  _StartSubscreenState createState() => _StartSubscreenState();
}

class _StartSubscreenState extends State<StartSubscreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Icon(Icons.home),
    );
  }
}
