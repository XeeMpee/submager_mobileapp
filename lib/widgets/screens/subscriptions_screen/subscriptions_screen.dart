import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:submager/repositories/payments_repository.dart';
import 'package:submager/widgets/screens/subscriptions_screen/widgets/subscription_tile_widget.dart';

class SubscriptionsScreen extends StatelessWidget {
  SubscriptionsScreen({Key? key}) : super(key: key);
  // final PaymentsRepository paymentsRepository = PaymentsRepository();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // body: StreamBuilder<QuerySnapshot>(
        //     stream: paymentsRepository.getStream(),
        //     builder: (context, snapshot) {
        //       List<dynamic> data;
        //       if (!snapshot.hasData) return LinearProgressIndicator();
        //       print("data");
        //       print(snapshot.data);

        body: Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: ListView(
        children: [
          SubscriptionTileWidget(
            subscriptionName: "Spotiy",
            date: "12.02.12",
            amount: "19.90",
          ),
          SubscriptionTileWidget(
            subscriptionName: "Netflix",
            date: "20.12.15",
            amount: "53.50",
          )
        ],
      ),
    )
        // })
        );
  }
}
