import 'package:flutter/material.dart';

class SubscriptionTileWidget extends StatefulWidget {
  SubscriptionTileWidget({Key? key,  required this.subscriptionName, required this.date, required this.amount}) : super(key: key);

  String subscriptionName;
  String date;
  String amount;
  @override
  _SubscriptionTileWidgetState createState() => _SubscriptionTileWidgetState();
}

class _SubscriptionTileWidgetState extends State<SubscriptionTileWidget> {
  bool isActive = false;
  @override
  Widget build(BuildContext context) {
    return   ListTile(
      leading: CircleAvatar(foregroundColor: Colors.amber),
      title: _buildSubscriptionElementTitle(widget.subscriptionName, widget.date),
      trailing: _buildTrailingElementTitle(widget.amount),
    );
  }

  Widget _buildSubscriptionElementTitle(String title, String addedAt) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            title,
            style: TextStyle(fontFamily: "PKOFont", fontSize: 30),
          ),
          Text(
            addedAt,
            style: TextStyle(fontFamily: "PKOFont", fontSize: 15),
          )
        ]);
  }

  Widget _buildTrailingElementTitle(String amount) {
    return Container(
      width: 150,
      child: Row(
        children: [
          Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  amount,
                  style: TextStyle(fontFamily: "PKOFont", fontSize: 25),
                ),
                Text("miesięcznie",style: TextStyle(fontFamily: "PKOFont", fontSize: 15),)
              ]),
          Container(height: 10, child: Switch(value: isActive, onChanged: (bool state) {
            setState(() {
              isActive = state;
            });
          }))
        ],
      ),
    );
  }
}